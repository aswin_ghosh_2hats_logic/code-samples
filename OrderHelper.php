<?php

namespace App\Helpers;

use Exception;
use Illuminate\Support\Facades\Log;

class OrderHelper
{
    public static function calculateAndUpdateOrderPrice($order)
    {
        try {
            $orderTotalPrice = $order->OrderProducts->sum(function ($product) {
                return in_array($product->offer_status, array('', 'approved'))
                ?  $product->order_price * $product->quantity : 0;
            });
            $order->total_price = $orderTotalPrice;
            $order->saveQuietly(); 
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }
    }
}
