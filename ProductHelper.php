<?php

namespace App\Helpers;

use Exception;
use Illuminate\Support\Facades\Log;

class ProductHelper
{
    public static function calculateAndUpdateTotalQuantity($product)
    {
        try {
            $totalProductQuantity = $product->productWarehouse->sum(function ($warehouseStock) {
                return $warehouseStock->quantity;
            });
            $product->quantity = $totalProductQuantity;
            $product->saveQuietly(); 
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }
    }
}
