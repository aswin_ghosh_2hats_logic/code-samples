<?php

namespace App\Observers;

use App\Mail\OrderStatusChange as MailOrderStatusChange;
use App\Models\Order;
use App\Models\OrderStatusChange;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class OrderObserver
{
    /**
     * Handle the Order "created" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function created(Order $order)
    {
        //
    }

    /**
     * Handle the Order "updated" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function updating(Order $order)
    {
        $order->last_updated_by = auth()->user()->id ?? null;
    }

    /**
     * Handle the Order "updated" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function updated(Order $order)
    {
        if ($order->isDirty('order_status_id')) {
            OrderStatusChange::create([
                'order_id' => $order->id,
                'status_id' => $order->getOriginal('order_status_id'),
                'status_to_id' => $order->order_status_id,
                'staff_id' => auth()->user()->id ?? null,
            ]);

            try {
                $subject    = 'The status of your order is - ' . $order->orderStatus->status;
                $message    = '<p>The status of your order(#'. $order->id .') is changed to ' . $order->orderStatus->status . '</p>';
                Mail::to($order->customer->email)
                    ->send(new MailOrderStatusChange($subject, $message));
            } catch (Exception $exception) {
                $errorLog = array(
                    'message' => $exception->getMessage(),
                    'action'  => 'Order status change mail sending',
                    'file'    => 'OrderObserver.php',
                );
                Log::error(json_encode($errorLog));
            }
        }
    }

    /**
     * Handle the Order "deleted" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function deleted(Order $order)
    {
        //
    }

    /**
     * Handle the Order "restored" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function restored(Order $order)
    {
        //
    }

    /**
     * Handle the Order "force deleted" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function forceDeleted(Order $order)
    {
        //
    }
}
